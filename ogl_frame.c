/*
 * Copyright ©2023 Alastair Growcott (AlastairGrowcott@hushmail.com). All
 * Rights Reserved. Permission to use, copy, modify, and distribute this
 * software and its documentation for educational, research, and
 * not-for-profit purposes, without fee and without a signed licensing
 * agreement, is hereby granted, provided that the above copyright notice,
 * this paragraph and the following two paragraphs appear in all copies,
 * modifications, and distributions.
 * 
 * IN NO EVENT SHALL ALASTAIR GROWCOTT BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
 * PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF ALASTAIR GROWCOTT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * 
 * ALASTAIR GROWCOTT SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * # BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * ALASTAIR GROWCOTT HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include <png.h>

#include "glad.h"
#include <GLFW/glfw3.h>

#include "ogl_frame.h"


typedef enum
{
    SHADER_TYPE_COLOUR,
    SHADER_TYPE_TEXTURE
} ShaderType;

typedef enum
{
    /* Values here must match the shader code. */
    BUFFER_TYPE_VERTEX   = 0u,
    BUFFER_TYPE_COLOUR   = 1u,
    BUFFER_TYPE_UV_COORD = 2u
} BufferType;


struct OGL_FRAME_PixmapData
{
    uint8_t *data;
    GLuint   handle;    
};


static int         f_init_count  = 0;
static int         f_running     = false;
static GLFWwindow *f_window      = NULL;
static int         f_width       = 0;
static int         f_height      = 0;
static float       f_fwidth      = 0.0;
static float       f_fheight     = 0.0;
static float       f_colour[3]   = { 1.0, 1.0, 1.0 };
static GLuint      f_col_program = 0u;
static GLuint      f_tex_program = 0u;
static GLuint      f_tex_id      = 0u;
static GLuint      f_vao         = 0u;
static GLuint      f_buffers[3]  = { 0u, 0u, 0u };


static const char *f_col_vertex_shader = "\
#version 430 core\n\
layout(location = 0) in vec2 position;\n\
layout(location = 1) in vec3 vert_colour;\n\
out vec3 frag_colour;\n\
void main(){\n\
    gl_Position = vec4(position, 0.0, 1.0);\n\
    frag_colour = vert_colour;\n\
}\n\
";

const char *f_col_fragment_shader = "\
#version 430 core\n\
in vec3 frag_colour;\n\
out vec4 colour;\n\
void main()\n\
{\n\
    colour = vec4(frag_colour, 1.0);\n\
}\n\
";

const char *f_tex_vertex_shader = "\
#version 430 core\n\
layout(location = 0) in vec2 position;\n\
layout(location = 2) in vec2 vert_uv;\n\
out vec2 frag_uv;\n\
void main(){\n\
    gl_Position = vec4(position, 0.0, 1.0);\n\
    frag_uv = vert_uv;\n\
}\n\
";

const char *f_tex_fragment_shader = "\
#version 430 core\n\
in vec2 frag_uv;\n\
uniform sampler2D texture_data;\n\
out vec4 colour;\n\
void main()\n\
{\n\
    colour = texture(texture_data, frag_uv);\n\
}\n\
";


static inline float SCALE_XCOORD(int c)
{
    float fc = (float)c;
    return ((fc * 2.0) / f_fwidth) - 1.0;
}


static inline float SCALE_YCOORD(int c)
{
    float fc = (float)c;
    return 1.0 - ((fc * 2.0) / f_fheight);
}


static int compile_shader(GLenum type, const char *script, GLuint *handle)
{
    int      result         = 0;
    GLint    compile_status = GL_FALSE;
    GLsizei  log_size;
    char    *compile_error;

    *handle = glCreateShader(type);
    glShaderSource(*handle, 1, &script, NULL);
    glCompileShader(*handle);
    glGetShaderiv(*handle, GL_COMPILE_STATUS, &compile_status);
    if (GL_FALSE == compile_status)
    {
        glGetShaderiv(*handle, GL_INFO_LOG_LENGTH, &log_size);
        compile_error = malloc(log_size);
        glGetShaderInfoLog(*handle, log_size, &log_size, compile_error);
        fprintf(stderr, "Failed to compile shader: %s\n", compile_error);
        free(compile_error);
        glDeleteShader(*handle);
        *handle = 0u;
        result = -1;
    }

    return result;
}


static int compile_program(ShaderType type, GLuint *program)
{
    int         result             = 0;
    const char *vertex_shader      = NULL;
    const char *frag_shader        = NULL;
    GLuint      vert_shader_handle = 0u;
    GLuint      frag_shader_handle = 0u;
    GLint       link_status        = GL_FALSE;
    GLsizei     log_size;
    char       *link_error;

    switch (type)
    {
        case SHADER_TYPE_COLOUR:
            vertex_shader = f_col_vertex_shader;
            frag_shader = f_col_fragment_shader;
            break;

        case SHADER_TYPE_TEXTURE:
            vertex_shader = f_tex_vertex_shader;
            frag_shader = f_tex_fragment_shader;
            break;

        default:
            fprintf(stderr, "Internal error, shader type %d not known.\n", type);
            result = -1;
            break;
    }

    if (0 == result) {
        result = compile_shader(GL_VERTEX_SHADER, vertex_shader, &vert_shader_handle);
    }

    if (0 == result) {
        result = compile_shader(GL_FRAGMENT_SHADER, frag_shader, &frag_shader_handle);
    }

    if (0 == result) {
        *program = glCreateProgram();
        glAttachShader(*program, vert_shader_handle);
        glAttachShader(*program, frag_shader_handle);
        glLinkProgram(*program);
        glGetProgramiv(*program, GL_LINK_STATUS, &link_status);
        if (GL_FALSE == link_status)
        {
            glGetProgramiv(*program, GL_INFO_LOG_LENGTH, &log_size);
            link_error = malloc(log_size);
            glGetProgramInfoLog(*program, log_size, &log_size, link_error);
            fprintf(stderr, "Failed to link shaders: %s\n", link_error);
            free(link_error);
            result = -1;
        }
    }

    if (0u != *program)
    {
        glDetachShader(*program, vert_shader_handle);
        glDetachShader(*program, frag_shader_handle);
    }

    if (0u != frag_shader_handle)
    {
        glDeleteShader(frag_shader_handle);
    }

    if (0u != vert_shader_handle)
    {
        glDeleteShader(vert_shader_handle);
    }

    if ((0 != result) && (0u != *program))
    {
        glDeleteProgram(*program);
        *program = 0u;
    }

    return result;
}


static bool load_png(const char *filepath, uint8_t **pixels, uint8_t *pixel_size, uint32_t *width, uint32_t *height)
{
    bool            result       = false;
    FILE           *fp           = NULL;
    png_byte        magic_buf[8];
    png_structp     png_ptr      = NULL;
    png_infop       info_ptr     = NULL;
    int             bit_depth;
    int             colours;
    png_bytep      *rows         = NULL;
    png_uint_32     i;

    (*pixel_size) = 0u;

    fp = fopen(filepath, "rb");
    if (NULL == fp) {
        fprintf(stderr, "Unable to open input file '%s'.\n", filepath);
    }
    else if (8 != fread (magic_buf, 1, 8, fp))
    {
        fprintf(stderr, "Unable to read input file '%s'.\n", filepath);
    }
    else if (0 != png_sig_cmp(magic_buf, 0, 8))
    {
        fprintf(stderr, "Input file '%s' is not PNG format.\n", filepath);
    }
    else
    {
        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        if (NULL == png_ptr)
        {
            fprintf(stderr, "Failed to read PNG file '%s'.\n", filepath);
        }
        else
        {
            info_ptr = png_create_info_struct(png_ptr);
            if (NULL == info_ptr)
            {
                fprintf(stderr, "Failed to read PNG file '%s'.\n", filepath);
            }
            else if (setjmp(png_jmpbuf(png_ptr)))
            {
                fprintf(stderr, "Failed to read PNG file '%s'.\n", filepath);
            }
            else
            {
                png_init_io(png_ptr, fp);
                png_set_sig_bytes(png_ptr, 8u);
                png_read_info(png_ptr, info_ptr);

                bit_depth = png_get_bit_depth(png_ptr, info_ptr);
                colours = png_get_color_type(png_ptr, info_ptr);

                switch (colours)
                {
                    case PNG_COLOR_TYPE_PALETTE:
                        png_set_palette_to_rgb(png_ptr);
                        break;

                    case PNG_COLOR_TYPE_GRAY:
                        if (8 > bit_depth)
                        {
                            png_set_expand_gray_1_2_4_to_8(png_ptr);
                        }
                        break;

                    default:
                        break;
                }

                if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
                {
                    png_set_tRNS_to_alpha(png_ptr);
                }

                if (16 == bit_depth)
                {
                    png_set_strip_16(png_ptr);
                }
                else
                {
                    if (8 > bit_depth)
                    {
                        png_set_packing (png_ptr);
                    }
                }

                png_read_update_info(png_ptr, info_ptr);

                png_get_IHDR(png_ptr, info_ptr, width, height, &bit_depth, &colours, NULL, NULL, NULL);
                
                switch (colours)
                {
                    case PNG_COLOR_TYPE_GRAY:
                        (*pixel_size) = 1u;
                        break;

                    case PNG_COLOR_TYPE_GRAY_ALPHA:
                        (*pixel_size) = 2u;
                        break;

                    case PNG_COLOR_TYPE_RGB:
                        (*pixel_size) = 3u;
                        break;

                    case PNG_COLOR_TYPE_RGB_ALPHA:
                        (*pixel_size) = 4u;
                        break;

                    default:
                        fprintf(stderr, "Invalid colour type in PNG file '%s'.\n", filepath);
                        break;
                }
            }
        }
    }

    if (0u != (*pixel_size))
    {
        *pixels = (uint8_t *)malloc((*width) * (*height) * (*pixel_size));
        rows = (png_bytep *)malloc((*height) * sizeof(png_bytep));
        for (i = 0u; i < (*height); i++)
        {
            rows[i] = &((*pixels)[i * (*width) * (*pixel_size)]);
        }

        png_read_image(png_ptr, rows);
        png_read_end(png_ptr, NULL);

        free(rows);

        result = true;
    }

    if (NULL != info_ptr)
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    }
    else
    {
        if (NULL != png_ptr)
        {
            png_destroy_read_struct(&png_ptr, NULL, NULL);
        }
    }

    if (NULL != fp)
    {
        fclose(fp);
    }

    return result;
}


static void free_all(void)
{
    if (0u != f_buffers[0])
    {
        glDeleteBuffers(3, f_buffers);
    }

    if (0u != f_vao)
    {
        glBindVertexArray(f_vao);
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);
        glDeleteVertexArrays(1, &f_vao);
        f_vao = 0u;
    }

    if (0u != f_col_program)
    {
        glDeleteProgram(f_col_program);
        f_col_program = 0u;
    }

    if (0u != f_tex_program)
    {
        glDeleteProgram(f_tex_program);
        f_tex_program = 0u;
    }

    f_tex_id = 0u;

    if (NULL != f_window)
    {
        glfwDestroyWindow(f_window);
        f_window = NULL;
    }

    glfwTerminate();
}

static void on_resize(GLFWwindow *window, int width, int height)
{
    glfwGetFramebufferSize(f_window, &f_width, &f_height);
    glViewport(0, 0, f_width, f_height);
    f_fwidth = (float)f_width;
    f_fheight = (float)f_height;
}


int OGL_FRAME_Init(int width, int height)
{
    int                result  = 0;
    GLFWmonitor       *monitor = NULL;
    const GLFWvidmode *mode;
    int                cursor  = GLFW_CURSOR_NORMAL;

    if (0 == f_init_count)
    {
        if (!glfwInit())
        {
            fprintf(stderr, "Failed to initialise GLFW.\n");
            result = -1;
        }
        else
        {
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT,  GL_TRUE);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,  4);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,  1);
            glfwWindowHint(GLFW_OPENGL_PROFILE,         GLFW_OPENGL_CORE_PROFILE);
            glfwWindowHint(GLFW_DOUBLEBUFFER,           GL_TRUE);
            glfwWindowHint(GLFW_DECORATED,              GL_TRUE);
            glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT,   GLFW_FALSE);
            glfwWindowHint(GLFW_RED_BITS,               8);
            glfwWindowHint(GLFW_GREEN_BITS,             8);
            glfwWindowHint(GLFW_BLUE_BITS,              8);
            glfwWindowHint(GLFW_ALPHA_BITS,             8);
            glfwWindowHint(GLFW_DEPTH_BITS,             24);
            glfwWindowHint(GLFW_STENCIL_BITS,           GLFW_DONT_CARE);

            monitor = glfwGetPrimaryMonitor();
            mode = glfwGetVideoMode(monitor);

            if ((0 > width) || (0 > height))
            {
                width = mode->width;
                height = mode->height;
                cursor = GLFW_CURSOR_HIDDEN;
            }
            else
            {
                if (0 == width)
                {
                    width = mode->width;
                }
                if (0 == height)
                {
                    height = mode->height;
                }
                monitor = NULL;
            }

            f_width = width;
            f_height = height;
            f_fwidth = (float)f_width;
            f_fheight = (float)f_height;

            f_window = glfwCreateWindow(width, height, "", monitor, NULL);
            if (NULL == f_window)
            {
                fprintf(stderr, "Failed to create window.\n");
                result = -1;
            }
        }

        if (0 == result)
        {
            glfwMakeContextCurrent(f_window);
            glfwSwapInterval(1);
            (void)glfwSetFramebufferSizeCallback(f_window, on_resize);
            glfwSetInputMode(f_window, GLFW_CURSOR, cursor);
            if (0 == gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
            {
                fprintf(stderr, "Failed to load OpenGL.\n");
                result = -1;
            }
        }

        if (0 == result)
        {
            result = compile_program(SHADER_TYPE_COLOUR, &f_col_program);
        }

        if (0 == result)
        {
            result = compile_program(SHADER_TYPE_TEXTURE, &f_tex_program);
        }

        if (0 == result)
        {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            f_tex_id = glGetUniformLocation(f_tex_program, "texture_data");

            glGenVertexArrays(1, &f_vao);
            glBindVertexArray(f_vao);
            glGenBuffers(3, f_buffers);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            glEnableVertexAttribArray(2);
        }
        else
        {
            free_all();
        }
    }

    if (0 == result)
    {
        f_init_count++;
    }

    return result;
}


void OGL_FRAME_Term(void)
{
    if (0 < f_init_count)
    {
        f_init_count--;

        if (0 == f_init_count)
        {
            free_all();
        }
    }
}


void OGL_FRAME_GetScreenSize(int *width, int *height)
{
    *width = f_width;
    *height = f_height;
}


void OGL_FRAME_SetBackColour(float red, float green, float blue)
{
    glClearColor(red, green, blue, 1.0);
}


void OGL_FRAME_SetForeColour(float red, float green, float blue)
{
    f_colour[0] = red;
    f_colour[1] = green;
    f_colour[2] = blue;
}


void OGL_FRAME_ClearScreen(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
}


void OGL_FRAME_RefreshScreen(void)
{
    glfwSwapBuffers(f_window);
}


void OGL_FRAME_DrawCircle(int centre_x, int centre_y, int radius)
{
    static int  num_segments     = 40;
    float       vertices[num_segments * 2];
    float       colours[num_segments * 3];
    float       centre_x_f       = SCALE_XCOORD(centre_x);
    float       centre_y_f       = SCALE_YCOORD(centre_y);
    float       theta            = (2.0 * 3.1415926) / num_segments;
    float       rx;
    float       ry;
    int         i;

    for (i = 0; i < num_segments; i++)
    {
        rx = ((float)radius * cos((float)i * theta) * 2.0) / f_width;
        ry = ((float)radius * sin((float)i * theta) * 2.0) / f_height;
        vertices[i * 2] = centre_x_f + rx;
        vertices[(i * 2) + 1] = centre_y_f + ry;
        colours[i * 3] = f_colour[0];
        colours[(i * 3) + 1] = f_colour[1];
        colours[(i * 3) + 2] = f_colour[2];
    }

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_COLOUR]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colours), colours, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_COLOUR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glUseProgram(f_col_program);
    glDrawArrays(GL_TRIANGLE_FAN, 0, num_segments);
}


void OGL_FRAME_DrawPentagon(int centre_x, int centre_y, int radius)
{
    static int  num_segments     = 5;
    float       vertices[num_segments * 2];
    float       colours[num_segments * 3];
    /*
     * When converting from pixels to screen co-ordinates...yeah, not nice. We start at one side
     * and then walk in a circle, so scale x by width and then that same scale should automatically
     * translate in y - we hope.
     */
    float      centre_x_f       = SCALE_XCOORD(centre_x);
    float      centre_y_f       = SCALE_YCOORD(centre_y);
    float      theta            = (2.0 * 3.1415926) / num_segments;
    float      rx;
    float      ry;
    int        i;

    for (i = 0; i < num_segments; i++)
    {
        rx = ((float)radius * cos((float)i * theta) * 2.0) / f_width;
        ry = ((float)radius * sin((float)i * theta) * 2.0) / f_height;
        vertices[i * 2] = centre_x_f + rx;
        vertices[(i * 2) + 1] = centre_y_f + ry;
        colours[i * 3] = f_colour[0];
        colours[(i * 3) + 1] = f_colour[1];
        colours[(i * 3) + 2] = f_colour[2];
    }

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_COLOUR]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colours), colours, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_COLOUR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glUseProgram(f_col_program);

    glDrawArrays(GL_TRIANGLE_FAN, 0, num_segments);
}


void OGL_FRAME_DrawRectangle(int x, int y, int width, int height)
{
    float vertices[8]   =
    {
        SCALE_XCOORD(x), SCALE_YCOORD(y),
        SCALE_XCOORD(x), SCALE_YCOORD(y + height),
        SCALE_XCOORD(x + width), SCALE_YCOORD(y + height),
        SCALE_XCOORD(x + width), SCALE_YCOORD(y)
    };
    float colours[12] =
    {
        f_colour[0], f_colour[1], f_colour[2],
        f_colour[0], f_colour[1], f_colour[2],
        f_colour[0], f_colour[1], f_colour[2],
        f_colour[0], f_colour[1], f_colour[2]
    };

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_COLOUR]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colours), colours, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_COLOUR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glUseProgram(f_col_program);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}


void OGL_FRAME_DrawTriangle(int x1, int y1, int x2, int y2, int x3, int y3)
{
    float vertices[6]   =
    {
        SCALE_XCOORD(x1), SCALE_YCOORD(y1),
        SCALE_XCOORD(x2), SCALE_YCOORD(y2),
        SCALE_XCOORD(x3), SCALE_YCOORD(y3)
    };
    float colours[9] =
    {
        f_colour[0], f_colour[1], f_colour[2],
        f_colour[0], f_colour[1], f_colour[2],
        f_colour[0], f_colour[1], f_colour[2]
    };

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_COLOUR]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colours), colours, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_COLOUR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glUseProgram(f_col_program);

    glDrawArrays(GL_TRIANGLES, 0, 3);
}


void OGL_FRAME_DrawTriangleFan(int count, int *x, int *y)
{
    int    vertices_size = count * 2 * sizeof(float);
    float *vertices      = (float *)malloc(vertices_size);
    int    colours_size  = count * 3 * sizeof(float);
    float *colours       = (float *)malloc(colours_size);
    int    i;

    for (i = 0u; i < count; i++)
    {
        vertices[i * 2] = SCALE_XCOORD(x[i]);
        vertices[(i * 2) + 1] = SCALE_YCOORD(y[i]);
        colours[i * 3] = f_colour[0];
        colours[(i * 3) + 1] = f_colour[1];
        colours[(i * 3) + 2] = f_colour[2];
    }

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, vertices_size, vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_COLOUR]);
    glBufferData(GL_ARRAY_BUFFER, colours_size, colours, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_COLOUR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glUseProgram(f_col_program);

    glDrawArrays(GL_TRIANGLE_FAN, 0, count);

    free(colours);
    free(vertices);
}


void OGL_FRAME_Start(OGL_FRAME_DRAW_FUNC callback)
{
    f_running = true;

    while (f_running)
    {
        callback();
        glfwSwapBuffers(f_window);

        glfwPollEvents();
        if (GLFW_TRUE == glfwWindowShouldClose(f_window))
        {
            f_running = false;
        }
    }
}


void OGL_FRAME_Stop(void)
{
    f_running = false;
}


bool OGL_FRAME_IsKeyPressed(int key)
{
    return (GLFW_PRESS == glfwGetKey(f_window, key)) ? true : false;
}


OGL_FRAME_Pixmap OGL_FRAME_CreatePixmap(const char *filepath)
{
    OGL_FRAME_Pixmap  result     = NULL;
    uint8_t          *data;
    uint8_t           pixel_size;
    uint32_t          width;
    uint32_t          height;
    GLint             alignment;
    GLint             int_format = GL_NONE;
    GLint             format     = GL_NONE;

    if (load_png(filepath, &data, &pixel_size, &width, &height))
    {
        switch (pixel_size)
        {
            case 1:
                int_format = GL_R8;
                format = GL_RED;
                break;

            case 2:
                int_format = GL_RG8;
                format = GL_RG;
                break;

            case 3:
                int_format = GL_RGB8;
                format = GL_RGB;
                break;

            case 4:
                int_format = GL_RGBA8;
                format = GL_RGBA;
                break;

            default:
                fprintf(stderr, "Invalid pixel size %d.\n", pixel_size);
                break;
        }

        if (GL_NONE != format)
        {
            result = (OGL_FRAME_Pixmap)malloc(sizeof(struct OGL_FRAME_PixmapData));
            result->data = data;
            glGenTextures(1, &(result->handle));
            glBindTexture(GL_TEXTURE_2D, result->handle);

            glGetIntegerv(GL_UNPACK_ALIGNMENT, &alignment);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

            glTexImage2D(GL_TEXTURE_2D, 0, int_format, width, height, 0, format, GL_UNSIGNED_BYTE, result->data);

            glPixelStorei(GL_UNPACK_ALIGNMENT, alignment);

            glGenerateMipmap(GL_TEXTURE_2D);
        } else {
            free(data);
        }
    }

    return result;
}


void OGL_FRAME_DestroyPixmap(OGL_FRAME_Pixmap pixmap)
{
    if (NULL != pixmap)
    {
        glDeleteTextures(1, &(pixmap->handle));
        free(pixmap->data);
        free(pixmap);
    }
}


void OGL_FRAME_DrawPixmap(OGL_FRAME_Pixmap pixmap, int x, int y, int width, int height)
{
    float vertices[8]   =
    {
        SCALE_XCOORD(x), SCALE_YCOORD(y),
        SCALE_XCOORD(x), SCALE_YCOORD(y + height),
        SCALE_XCOORD(x + width), SCALE_YCOORD(y + height),
        SCALE_XCOORD(x + width), SCALE_YCOORD(y)
    };
    float uv[8]         = { 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0 };

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, pixmap->handle);
    glUniform1i(f_tex_id, 0);

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_UV_COORD]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(uv), uv, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_UV_COORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glUseProgram(f_tex_program);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}


void OGL_FRAME_DrawPixmapVertices(OGL_FRAME_Pixmap pixmap, int *x, int *y)
{
    float vertices[8];
    float uv[8]       = { 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0 };
    int   i;

    for (i = 0; i < 4; i++)
    {
        vertices[i * 2] = SCALE_XCOORD(x[i]);
        vertices[(i * 2) + 1] = SCALE_YCOORD(y[i]);
    };

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, pixmap->handle);
    glUniform1i(f_tex_id, 0);

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, f_buffers[BUFFER_TYPE_UV_COORD]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(uv), uv, GL_STATIC_DRAW);
    glVertexAttribPointer(BUFFER_TYPE_UV_COORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glUseProgram(f_tex_program);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}


uint32_t OGL_FRAME_GetPixelColour(int x, int y)
{
    uint32_t  result = 0xFFFFFFFFu;

    glReadPixels(x, (f_height - y), 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &result);

    return result;
}


