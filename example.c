/*
 * Copyright ©2023 Alastair Growcott (AlastairGrowcott@hushmail.com). All
 * Rights Reserved. Permission to use, copy, modify, and distribute this
 * software and its documentation for educational, research, and
 * not-for-profit purposes, without fee and without a signed licensing
 * agreement, is hereby granted, provided that the above copyright notice,
 * this paragraph and the following two paragraphs appear in all copies,
 * modifications, and distributions.
 * 
 * IN NO EVENT SHALL ALASTAIR GROWCOTT BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
 * PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF ALASTAIR GROWCOTT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * 
 * ALASTAIR GROWCOTT SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * # BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * ALASTAIR GROWCOTT HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */


#include <stdio.h>
#include <math.h>
#include <GLFW/glfw3.h>

#include "ogl_frame.h"


static const double ROTATE_RATE = 0.05;


static double            f_x     = 50;
static double            f_y     = 200;
static double            f_angle = 0.0;
static OGL_FRAME_Pixmap  f_car   = NULL;


static void draw(void)
{
    int     w;
    int     h;
    double  sin_val;
    double  cos_val;
    int     x[4];
    int     y[4];
    int     px;
    int     py;

    if (OGL_FRAME_IsKeyPressed(GLFW_KEY_ESCAPE))
    {
        OGL_FRAME_Stop();
    }
    else
    {
        OGL_FRAME_GetScreenSize(&w, &h);
        OGL_FRAME_SetBackColour(0.5, 0.5, 1.0);
        OGL_FRAME_ClearScreen();
        OGL_FRAME_SetBackColour(0.0, 0.0, 0.0);

        OGL_FRAME_SetForeColour(1.0, 0.0, 0.0);
        OGL_FRAME_DrawCircle(h / 4, h / 4, h / 8);

        OGL_FRAME_SetForeColour(0.0, 1.0, 0.0);
        OGL_FRAME_DrawTriangle(w - (h / 4), h / 8, w - (h * 3 / 8), h * 3 / 8, w - (h / 8), h * 3 / 8);

        OGL_FRAME_SetForeColour(0.0, 0.0, 1.0);
        OGL_FRAME_DrawRectangle(50, 50, 50, 75);

        if (OGL_FRAME_IsKeyPressed(GLFW_KEY_RIGHT)) {
            f_angle += ROTATE_RATE;
        }

        if (OGL_FRAME_IsKeyPressed(GLFW_KEY_LEFT)) {
            f_angle -= ROTATE_RATE;
        }

        if (OGL_FRAME_IsKeyPressed(GLFW_KEY_UP)) {
            f_x += 5.0 * cos(f_angle);
            f_y += 5.0 * sin(f_angle);
        }

        if (f_x < -32) {
            f_x = 1952;
        }
        else
        {
            if (f_x > 1952) {
                f_x = -32;
            }
        }

        if (f_y < -32) {
            f_y = 1112;
        }
        else
        {
            if (f_y > 1112) {
                f_y = -32;
            }
        }

        sin_val = sin(f_angle) * -1;
        cos_val = cos(f_angle);
        x[0] = (int)(f_x - (16 * (cos_val + sin_val)));
        y[0] = (int)(f_y - (16 * (cos_val - sin_val)));
        x[1] = (int)(f_x - (16 * (cos_val - sin_val)));
        y[1] = (int)(f_y + (16 * (cos_val + sin_val)));
        x[2] = (int)(f_x + (16 * (cos_val + sin_val)));
        y[2] = (int)(f_y + (16 * (cos_val - sin_val)));
        x[3] = (int)(f_x + (16 * (cos_val - sin_val)));
        y[3] = (int)(f_y - (16 * (cos_val + sin_val)));

        px = (x[2] + x[3]) / 2;
        py = (y[2] + y[3]) / 2;
        if (0xFFFF7F7Fu != OGL_FRAME_GetPixelColour(px, py)) {
            printf("Crash!!\n");
            OGL_FRAME_Stop();
        }

        OGL_FRAME_DrawPixmapVertices(f_car, x, y);
    }
}


int main(int argc, char **argv)
{
    int result = 0;

    if (0 != OGL_FRAME_Init(-1, -1))
    {
        fprintf(stderr, "Failed to initialise OGL Framework.\n");
        result = -1;
    }
    else
    {
        f_car = OGL_FRAME_CreatePixmap("car_right.png");
        if (NULL == f_car)
        {
            fprintf(stderr, "Failed to load 'car_right.png'.\n");
            result = -1;
        }

        if (0 == result)
        {
            OGL_FRAME_Start(draw);
        }

        OGL_FRAME_DestroyPixmap(f_car);
        OGL_FRAME_Term();
    }

    return result;
}


