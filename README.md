# OpenGL Framework

A simple OpenGL graphics library for teaching simple graphics programming.

## Introduction

When I was young I was fortunate enough to go to a school that had a computer club and provided computers (BBC Model Bs) for our use. We weren't allowed to play games which, of course, is what we all really wanted to do. But we were allowed to test our programs. So of course we spent all our time playing computer games.

I am now Head of Software Development and have had a very successful career as a software engineer. I also have children and, wanting them to follow in my footsteps as we all do, I wanted to encourage them to program. Of course things have changed now and the simple functionality we enjoyed on the BBC Model Bs is not really available on a modern home computer. The sophisitcation of modern graphics means that there is a lot of complexity in simply displaying a window to the user, let alone drawing on it.

I wrote this library to provide such a simple API. A single initialisation call, and single functions to draw circles and rectangles. It has grown slightly since then as my son wanted to write a game and I found myself wanting more out of this library. I expect it will grow more in future too.



