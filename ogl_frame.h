/*
 * Copyright ©2023 Alastair Growcott (AlastairGrowcott@hushmail.com). All
 * Rights Reserved. Permission to use, copy, modify, and distribute this
 * software and its documentation for educational, research, and
 * not-for-profit purposes, without fee and without a signed licensing
 * agreement, is hereby granted, provided that the above copyright notice,
 * this paragraph and the following two paragraphs appear in all copies,
 * modifications, and distributions.
 * 
 * IN NO EVENT SHALL ALASTAIR GROWCOTT BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
 * PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF ALASTAIR GROWCOTT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * 
 * ALASTAIR GROWCOTT SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * # BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * ALASTAIR GROWCOTT HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */


#ifndef OGL_FRAME_H
#define OGL_FRAME_H


#include <stdbool.h>
#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Callback function to do drawing.
 *
 * The OpenGL framework module will call this callback on each drawing cycle.
 */
typedef void (*OGL_FRAME_DRAW_FUNC)(void);


/** @brief Handle to a loaded pixmap */
typedef struct OGL_FRAME_PixmapData *OGL_FRAME_Pixmap;


/**
 * @brief Initialise the OpenGL framework module.
 *
 * If width or height is set to -1, the window created will be fullscreen.
 *
 * @param[in]      width               The width of the screen.
 * @param[in]      height              The height of the screen.
 *
 * @retval 0  on success;
 * @retval -1 on failure.
 */
int OGL_FRAME_Init(int width, int height);


/**
 * @brief Terminate the OpenGL framework module.
 */
void OGL_FRAME_Term(void);


/**
 * @brief Get the size of the screen to be drawn on.
 *
 * If the module has not been initialised, the returned width and height will
 * be zero.
 *
 * @param[out]     width               The width of the screen.
 * @param[out]     height              The height of the screen.
 */
void OGL_FRAME_GetScreenSize(int *width, int *height);


/**
 * @brief Set the background colour.
 *
 * Colour values must be in the range 0.0-1.0 inclusive.
 *
 * @param[in]      red                 Amount of red to use in background.
 * @param[in]      green               Amount of green to use in background.
 * @param[in]      blue                Amount of blue to use in background.
 */
void OGL_FRAME_SetBackColour(float red, float green, float blue);


/**
 * @brief Set the foreground colour.
 *
 * Colour values must be in the range 0.0-1.0 inclusive.
 *
 * @param[in]      red                 Amount of red to use in background.
 * @param[in]      green               Amount of green to use in background.
 * @param[in]      blue                Amount of blue to use in background.
 */
void OGL_FRAME_SetForeColour(float red, float green, float blue);


/**
 * @brief Clear the screen.
 */
void OGL_FRAME_ClearScreen(void);


/**
 * @brief Draw a circle.
 *
 * @param[in]      centre_x            X-coordinate of centre.
 * @param[in]      centre_y            Y-coordinate of centre.
 * @param[in]      radius              X-coordinate of centre.
 */
void OGL_FRAME_DrawCircle(int centre_x, int centre_y, int radius);


/**
 * @brief Draw a Pentagon.
 *
 * @param[in]      centre_x            X-coordinate of centre.
 * @param[in]      centre_y            Y-coordinate of centre.
 * @param[in]      radius              X-coordinate of centre.
 */
void OGL_FRAME_DrawPentagon(int centre_x, int centre_y, int radius);


/**
 * @brief Draw a triangle.
 *
 * @param[in]      x, y                Co-ordinates of the first corner.
 * @param[in]      width               Width of the rectangle.
 * @param[in]      height              Height of the rectangle.
 */
void OGL_FRAME_DrawRectangle(int x, int y, int width, int height);


/**
 * @brief Draw a triangle.
 *
 * @param[in]      x1, y1              Co-ordinates of the first corner.
 * @param[in]      x2, y2              Co-ordinates of the second corner.
 * @param[in]      x3, y3              Co-ordinates of the third corner.
 */
void OGL_FRAME_DrawTriangle(int x1, int y1, int x2, int y2, int x3, int y3);


/**
 * @brief Draw a triangle fan.
 *
 * @param[in]      count               Number of vertices in triangle fan.
 * @param[in]      x                   Array of X co-ordinates.
 * @param[in]      y                   Array of Y co-ordinates.
 */
void OGL_FRAME_DrawTriangleFan(int count, int *x, int *y);


/**
 * @brief Start drawing.
 *
 * Drawing will finish when OGL_FRAME_Stop() is called.
 *
 * @param[in]      callback            Callback to do any actual drawing.
 */
void OGL_FRAME_Start(OGL_FRAME_DRAW_FUNC callback);


/**
 * @brief Stop drawing.
 */
void OGL_FRAME_Stop(void);


/**
 * @brief Check if a key is pressed.
 *
 * Requires:
 *     #include <GLFW/glfw3.h>
 *
 * Input key values must be GLFW_KEY values as listed at https://www.glfw.org/docs/latest/group__keys.html.
 *
 * @param[in]      key                 Key value to test.
 *
 * @retval true if key is pressed.
 * @retval false if key is not pressed.
 */
bool OGL_FRAME_IsKeyPressed(int key);


/**
 * @brief Load a PNG file as a pixmap.
 *
 * OGL_FRAME_DestroyPixmap() must be called to free the pixmap.
 *
 * @param[in]      filepath            Path to the PNG file to load.
 *
 * @retval NULL on failure.
 * @return The loaded pixmap on success.
 */
OGL_FRAME_Pixmap OGL_FRAME_CreatePixmap(const char *filepath);


/**
 * @brief Destroy a previously loaded pixmap.
 *
 * @param[in]      pixmap              The pixmap to destroy.
 */
void OGL_FRAME_DestroyPixmap(OGL_FRAME_Pixmap pixmap);


/**
 * @brief Draw a previously loaded pixmap.
 *
 * @param[in]      pixmap              The pixmap to draw.
 * @param[in]      x, y                Co-ordinates of the top-left corner.
 * @param[in]      width               Width of the pixmap.
 * @param[in]      height              Height of the pixmap.
 */
void OGL_FRAME_DrawPixmap(OGL_FRAME_Pixmap pixmap, int x, int y, int width, int height);


/**
 * @brief Draw a previously loaded pixmap with arbitrary vertices.
 *
 * The co-ordinates should be in the order:
 *     Top left.
 *     Bottom left.
 *     Bottom right.
 *     Top right.
 *
 * @param[in]      pixmap              The pixmap to draw.
 * @param[in]      x                   Array of 4 x c-ordinates.
 * @param[in]      y                   Array of 4 y c-ordinates.
 */
void OGL_FRAME_DrawPixmapVertices(OGL_FRAME_Pixmap pixmap, int *x, int *y);


/**
 * @brief Get the colour of a specified pixel on the screen.
 *
 * @param[in]      x                   X c-ordinate to get pixel for.
 * @param[in]      y                   Y c-ordinate to get pixel for.
 */
uint32_t OGL_FRAME_GetPixelColour(int x, int y);


#ifdef __cplusplus
}
#endif


#endif /* !OGL_FRAME_H */

